<?php
/**
 * Created by PhpStorm.
 * User: coyote69
 * Date: 6/12/17
 * Time: 10:37 AM
 */
header('Access-Control-Allow-Origin: *');

if(isset($_GET['time'])){
    $timezone  = -4; //(GMT -5:00) EST (U.S. & Canada)
    echo gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));
}
?>